---
marp: true
theme: base-theme
---

<!-- global styling -->
<style>
    @import url('https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap');

    h1, h2, h3, h4, h5 {
        /* we4it green */
        color: #91BD32;
        /*text-shadow: 1px 1px 4px #aaa; <- does not work in PDF */
    }

    h1 {
        font-size: 1.4em;
    }

    h2 {
        font-size:1.2em;
    }

    h6 {
        font-size: 0.8em;
    }

    section {
        background-image: url(images/notescamp.png);background-repeat: no-repeat;
        background-position: top 10px right 10px;
        background-size: 150px;
        font-family: 'Roboto Slab';
    }

    section footer {
      font-size: 0.5em;
      color: #888;
    }

    section.table-flat table {
        border: none;
        background: none;
        width: 100%;
    }

    section.table-flat table tr {
        border: none;
        background: none;
    }

    section.table-flat table td, section.table-flat table th {
        padding: 0px;
        background: transparent;
        text-align: center;
        border: none;
        width: 400px;
    }
    section.table-flat table td {
        font-size: 0.8em;
    }

    code {
        background-color: #ddd;
    }

    pre {
        font-size: 0.8em;
        background-color: #eee;
    }
</style>

<!-- paginate: false -->

![bg left:33% opacity:30%](images/bg.jpg)

# HCL Domino Volt - der NSF Killer?

![](images/hclvolt.png)

---

<!-- paginate: true -->
<!-- footer: NotesCamp '21 - HCL Domino Volt - der NSF Killer? -->

# Oliver Busse

![width:300px](images/we4it-logo.png)

![bg right:40%](images/obusse2021.png)


- Notes/Domino seit R4.5
- *Aveedo* &reg;
  - [https://www.aveedo.com/de/](https://www.aveedo.com/de/)
- *Mailissa* &reg;
  - [https://mailissa.com](https://mailissa.com)

![](images/hclamb.png)

---

# Agenda

- Was ist HCL Domino Volt?
- Zielgruppe
- Wie komme ich da ran?
- Wir bauen eine Anwendung
- Hinter den Kulissen
- Dokumentation
- Erweiterte Konfigurationen

---

# Was ist HCL Domino Volt?

- Low-Code/No-Code Umgebung von HCL für Domino
- Verwandt mit HCL Leap (läuft nicht auf Domino)
- Läuft zu 100% im Browser
- Servlet, muss separat auf Domino installiert werden
- Ablage der Daten und des App-Codes erfolgt in NSFs

---

<!-- footer: Wer das lesen kann braucht keine Brille -->

![bg right:33%](images/carol-beer-computer-says-no.jpg)

# <!-- fit --> HCL Domino Volt ≠ HCL Volt MX

---

<!-- footer: NotesCamp '21 - HCL Domino Volt - der NSF Killer? -->

# <!-- fit --> Exkurs: HCL Volt MX

---

# Temenos Visualizer

![](images/temenos_visualizer.png)

---

![bg left:33%](images/hacker.jpeg)

# Zielgruppe

- Notes-Entwickler
- Admins
- "Citizen Developer"

---

![bg right](images/howtoget.jpg)

# Wie komme ich da ran?

---

<!-- footer: Stand: Mai 2021 -->
<!-- _class: table_flat -->

# Verschiedene Optionen

![bg right height:70%](images/volt_downloads.png)

- Linux & Windows
- Docker Image
- Download via FlexNet
  - HCL Domino CCB (+)
  - HCL Domino CCX (inkl.)
  - HCL Domino Utility

---

<!-- footer: NotesCamp '21 - HCL Domino Volt - der NSF Killer? -->

# Installer

- Windows & Linux: Installation mittels Batch/Shell Script
- Dauer: 3 Fragen beantworten, 10 Sekunden Dateien kopieren + HTTP Neustart

---

# Docker Image

- Docker Image laden
- Docker Volume erzeugen
- *optional*: Demo-Daten benutzen
- Docker Container erzeugen und starten
- Volt Config zumindest sichten, ggf. Einstellungen vornehmen

---

# Docker Container mit Demo-Umgebung (1)

```
docker load --input volt-docker-V1101FP3-1.0.3.21.tgz
```

```
docker volume create voltdata
```

```
docker run --rm -i --user="0:0" \
-v voltdata:/local/notesdata --entrypoint "/bin/bash" \
volt-docker:V1101FP3-1.0.3.21 -c \
"cd /local/notesdata; tar xjf - <&0; chown -R 1000:root /local/notesdata" \
< renovations-data-volt-1.0.1.tbz2
```

```
docker run -d --name dominovolt \
-v voltdata:/local/notesdata \
--hostname dominovolt.local \
--cap-add=SYS_PTRACE \
-p 1352:1352 -p 80:80 \
volt-docker:V1101FP3-1.0.3.21
```

---

# Docker Container mit Demo-Umgebung (2)

![](images/voltconfig.png)

![](images/serveruri.png)

- HTTP durchstarten
- Und nun *http://<hostname>/volt-apps/secure/org/ide/manager.html* aufrufen
- Empfohlen: Website-Redirect einrichten

---

# Hallo, Volt!

![](images/volthome.png)

---

![bg right:75%](images/demo.jpg)

# <!-- fit --> Demo

---

# Wir bauen eine Anwendung

![](images/stoeps.png)

[https://twitter.com/stoeps/status/1385986592544800769](https://twitter.com/stoeps/status/1385986592544800769)

- Man nehme: eine Excel-Tabelle
  - In Zeile 1: Feldnamen
  - Ab Zeile 2: Daten

---

# <!-- fit --> Demo

---

![bg right:66%](images/behind.jpg)

# Hinter den Kulissen

---

![bg right fit](images/nsfs.png)

# Volt Config

- Befindet sich wie alle anderen NSF in Data/volt
- Enthält diverse Dokumente zur Steuerung von Volt
- Per Default sind alle inaktiv
- Mindestens "serverURI" sollte angepasst und aktiviert werden
- KEINE REPLIKEN VERWENDEN!

---

![bg right fit](images/nsfs.png)

# Volt Builder

- Enthält je Anwendung in Volt ein Dokument
  - definiert den Zugriff
  - enthält eine XML-Variante der Anwendung (Struktur) - Nicht editieren!

---

![bg right fit](images/nsfs.png)

# Volt Application History

- Enthält Log-Dokumente zum Status der jeweiligen Applikationen
- Referenz erfolgt nur über die App-ID
- Ganz hilfreich, um Aktivitäten der "Entwicker" zu überwachen

---

# Hinter den Kulissen: Anwendungs-Container

- Jede Volt-Anwendung wird als NSF gespeichert
  - enthält sowohl Design als auch Daten
  - Dateiname entspricht der App-ID
- Basis ist das Volt App Template (voltapp.ntf)
- Design-Elemente (Forms, Views) werden von der Runtime generiert
- Die Devise lautet: in Domino Designer öffnen und lernen, nicht bearbeiten!
- Dual-Betrieb möglich, dann aber Daten & Design trennen

---

# <!-- fit --> Demo

---

![bg left 100%](images/itscomplicated.png)

# Dokumentation

---

# Dokumentation

- "gut" verteilt
  - Websites
  - Wiki
  - README Dateien
  - Notes-Masken in der Volt Config
- Offizielle Docs teilweise nicht auf dem aktuellen Stand (zumindest nominal), funktionieren aber
- Entwicklung des Produkts ist agiler als die Dokumentation
- Tutorials, Webinars
- Bester Anlauf: das Wiki

---

# Wo geht's lang?

![](images/volthelp.png)

- Link zum Forum (Domino, Volt ist ein Sub-Forum)
- Link zu den Docs
- Link zum Wiki

---

![bg right](images/construction.jpeg)

# Erweiterte Konfigurationen

- Zugriffe & Rollen festlegen
- Anonymer Zugriff
- Volt Config einstellen
  - Javascript Security
  - REST Service Whitelist
- Styling
  - App
  - Global

---

# Zugriffe & Rollen festlegen

- Rollen verhalten sich ähnlich wie Gruppen in Domino
- Rollen haben "Mitglieder", das sind Nutzer und Gruppen aus Domino NAB
- Nur mit Rollen wird in Volt der Zugriff gesteuert
- verwirrend, da wir als "Notes'ler" mit Rollen etwas anderes verbinden

---

# Anonymer Zugriff

- Rolle "Initiator" (vorhanden bei neuen Apps, andernfalls erzeugen)
- "Anonyme Benutzer" zu "Initiator"-Rolle hinzufügen

---

# Volt Config einstellen

- für die meisten Demo-Apps braucht es "unsecure" Javascript
- für externe REST Services muss die entsprechende URL auf eine Whitelist gesetzt werden

---

# Styling

- nur für die App
  - mittels "Custom CSS"
- global
  - mittels Referenz auf ein zentrales CSS

---

# <!-- fit --> Demo

---

![bg left](images/chain.png)

# <!-- fit --> Exkurs: HCL Link

![](images/hcllink_logo.png)

---

# HCL Link

- eigenständiges Produkt
- Volt kann es benutzen
- Tool zum Erzeugen von APIs und Datenintegration
- bindet diverse externe Systeme an (tbd)
- "HEI on steroids"
- Nach Link Neustart muss Domino HTTP auch neu gestartet werden, sonst Fehler
- https://www.hcltechsw.com/products/link

---

# HCL Link - Schnellinstallation (Docker)

- HCL Link Design Server für Linux laden
  - enthält Install-Script und Docker Images
- Script für Konfiguration ausführen
- Script für Installation ausführen

---

# HCL Link - Konfiguration, Installation, Start, Stop

```
tar -xvf lnk_design_server_1.1.1.0_linux.tar.gz
```

```
./Link configure -t docker -i oem -u root
```

```
./Link install
```

```
./Link <start|stop>
```

Der Start benötigt ca. 5 Minuten!

---

# <!-- fit --> Demo

---

![bg left](images/fernrohr.jpg)

# <!-- fit --> Ausblick

---

#  Version 1.0.4

- geplant für September 2021
- neues UI/UX
- neuer visueller Workflow-Editor

---

![](images/wf104.png)

---

# Zeug

https://gitlab.com/obusse/notes-camp-21

https://help.hcltechsw.com/domino_volt/1.0.3/installing_volt.html

https://hclwiki.atlassian.net/wiki/spaces/HDV/pages/552960012/Sample+Apps

https://help.hcltechsw.com/domino_volt/1.0.1/ref_customized_css.html

---

<!-- footer: '' -->
<!-- paginate: false -->

![bg right height:350px](images/thanks.gif)

# <!-- fit -->Q & A