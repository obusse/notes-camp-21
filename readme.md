# NotesCamp 21 - Volt

## Abstract

HCL Domino Volt - der NSF Killer?
Mit der Low-Code-Erweiterung namens "Volt" hat HCL eine Alternative zur Entwicklung in der traditionellen NSF mit Domino Designer geschaffen. 

Die Zielgruppe ist hier der Power-User, der abseits von Excel-Tabellen kleine und einfache Applikationen erstellen kann und soll. Das läuft alles super einfach, bringt aber auch gewisse Probleme mit sich. 

Ich zeige, wie man Volt einsetzen kann und sollte, wie man die Sicherheit im Unternehmen weiterhin in gewohnter Notes-Manier gewährleistet, was man mit Volt alles anstellen kann sowie die Abgrenzung zu "HCL Volt MX".

Abseits der IDE zeige ich auch, was technisch dahinter steckt und wie man auch als Administrator und Maintainer unterstützend Einfluss nehmen kann. 

Dies ist keine Produkt-Demo sondern ein Use-Case mit allen Pros und Cons.
